const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
  res.send(
    'Nine coding challenge, Please post the payload to get the filtered response'
  );
});

router.post('/', (req, res, next) => {
  const body = req.body;
  const payload = body && body.payload;
  const response = [];

  if (Array.isArray(payload)) {
    payload.forEach(show => {
      if (show && show.drm === true && show.episodeCount > 0) {
        response.push({
          image: show.image && show.image.showImage,
          slug: show.slug,
          title: show.title
        });
      }
    });
  }
  return res.json({ response });
});

module.exports = router;
