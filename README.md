# 9Digital Coding Challenge

JSON-based express web service to process a payload of TV shows.

### Setup

```
git clone https://reenapatil@bitbucket.org/reenapatil/nine-coding-challenge.git
cd nine-coding-challenge
yarn install
```

### Running the application

```
DEBUG=nine-coding-challenge:* yarn start
```

### Run unit tests

```
yarn test
```
