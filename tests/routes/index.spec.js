const app = require('../../app');
const chai = require('chai');
const request = require('supertest');

const expect = chai.expect;

describe('POST /', function() {
  it('should return shows with drm and more than one episode', done => {
    request(app)
      .post('/')
      .send({
        payload: [
          {
            drm: true,
            episodeCount: 3,
            image: {
              showImage:
                'http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg'
            },
            slug: 'show/16kidsandcounting',
            title: '16 Kids and Counting'
          },
          {
            slug: 'show/seapatrol',
            title: 'Sea Patrol',
            tvChannel: 'Channel 9'
          }
        ]
      })
      .type('json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function(err, res) {
        const results = res.body.response;
        expect(results.length).to.be.equal(1);
        expect(results[0].title).to.be.equal('16 Kids and Counting');
        expect(results[0].slug).to.be.equal('show/16kidsandcounting');
        expect(results[0].image).to.be.equal(
          'http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg'
        );
        done();
      });
  });

  it('should not return the shows that without drm or without episodes', done => {
    request(app)
      .post('/')
      .send({
        payload: [
          {
            drm: true,
            image: {
              showImage:
                'http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg'
            },
            slug: 'show/16kidsandcounting',
            title: '16 Kids and Counting'
          },
          {
            episodeCount: 3,
            slug: 'show/seapatrol',
            title: 'Sea Patrol',
            tvChannel: 'Channel 9'
          }
        ]
      })
      .type('json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function(err, res) {
        const results = res.body.response;
        expect(results.length).to.be.equal(0);
        done();
      });
  });

  it('should return 400 for invalid json payload', done => {
    request(app)
      .post('/')
      .send('{"invalid"}')
      .type('json')
      .expect('Content-Type', /json/)
      .expect(400)
      .end(function(err, res) {
        expect(res.error.text).to.be.equal(
          '{"error":"Could not decode request: JSON parsing failed"}'
        );
        done();
      });
  });
});
